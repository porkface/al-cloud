package com.ruoyi.gen.controller;

import com.common.zrd.json.CommonJsonResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.PreAuthorize;
import com.ruoyi.gen.domain.TestCase;
import com.ruoyi.gen.service.ITestCaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 测试用例Controller
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@RestController
@RequestMapping("/case" )
public class TestCaseController extends BaseController {
    @Autowired
    private ITestCaseService testCaseService;

    /**
     * 查询测试用例列表
     */
    @PreAuthorize(hasPermi = "gen:case:list" )
    @GetMapping("/list" )
    public TableDataInfo list(TestCase testCase) {
        startPage();
        List<TestCase> list = testCaseService.selectTestCaseList(testCase);
        return getDataTable(list);
    }

    /**
     * 导出测试用例列表
     */
    @PreAuthorize(hasPermi = "gen:case:export" )
    @Log(title = "测试用例" , businessType = BusinessType.EXPORT)
    @PostMapping("/export" )
    public void export(HttpServletResponse response, TestCase testCase) throws IOException {
        List<TestCase> list = testCaseService.selectTestCaseList(testCase);
        ExcelUtil<TestCase> util = new ExcelUtil<>(TestCase.class);
        util.exportExcel(response, list, "case" );
    }

    /**
     * 获取测试用例详细信息
     */
    @PreAuthorize(hasPermi = "gen:case:query" )
    @GetMapping(value = "/{id}" )
    public AjaxResult getInfo(@PathVariable("id" ) Integer id) {
        return AjaxResult.success(testCaseService.getById(id));
    }

    /**
     * 新增测试用例
     */
    @PreAuthorize(hasPermi = "gen:case:add" )
    @Log(title = "测试用例" , businessType = BusinessType.INSERT)
    @PostMapping
    public CommonJsonResult add(@RequestBody TestCase testCase) {
        testCaseService.save(testCase);
        return CommonJsonResult.of(testCase);
    }

    /**
     * 修改测试用例
     */
    @PreAuthorize(hasPermi = "gen:case:edit" )
    @Log(title = "测试用例" , businessType = BusinessType.UPDATE)
    @PutMapping
    public CommonJsonResult edit(@RequestBody TestCase testCase) {
        testCaseService.updateById(testCase);
        return CommonJsonResult.of(testCase);
    }

    /**
     * 删除测试用例
     */
    @PreAuthorize(hasPermi = "gen:case:remove" )
    @Log(title = "测试用例" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}" )
    public CommonJsonResult remove(@PathVariable Integer[] ids) {

        testCaseService.removeByIds(Arrays.asList(ids));
        return CommonJsonResult.empty();
    }
}
