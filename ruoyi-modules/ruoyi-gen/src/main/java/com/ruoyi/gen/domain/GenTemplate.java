package com.ruoyi.gen.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.security.entity.BaseZrdEntity;
import com.ruoyi.common.translate.annotation.TranslateAnnotation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 代码生成模板管理1对象 gen_template
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenTemplate extends BaseZrdEntity implements Serializable {

    private static final long serialVersionUID = -9166500578771654364L;
    /**
     * 主键
     */
    private Long id;

    /**
     * 模板名称
     */
    @Excel(name = "模板名称" )
    private String name;

    /**
     * 文件名称
     */
    @Excel(name = "文件名称" )
    private String fileName;

    /**
     * 目标包路径
     */
    @Excel(name = "目标包路径" )
    private String targetPackage;

    /**
     * 模板内容
     */
    @Excel(name = "模板内容" )
    private String templateContent;

    /**
     * 方案ID
     */
    @Excel(name = "方案ID" )
    private Long schemeId;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 模板类型
     */
    @Excel(name = "模板类型" )
    @TranslateAnnotation(filed = "templateTypeName", distCode = "gen_type")
    private String templateType;



}
